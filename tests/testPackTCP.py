#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 01.02.2015
# Purpose: Тестирование протоколов P2+P3+P4+P5+P6+P7 Стека Коммуникационных 
# Протоколов (СКП)  
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import unittest
import os, sys
from fix_path import fix_sys_path
fix_sys_path(__file__)

if os.path.basename(os.getcwd()) == "web2py":
    # Тест запущен через import из модуля RunTest.py
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
#endif
#############################################################################
class TestPackTCP(unittest.TestCase):
    """
    Класс для тестирования протоколов P2+P3+P4+P5+P6+P7 
    Стека Коммуникационных Протоколов (СКП) 
    """
    #========================================================================
    def setUp(self):
        """
        Подготовительные процедуры
        """
        #--------------------------------------------------------------------
        # Пакет с запросом D+Nz+No+A+Z+KS
        self.lRq = [[6, 0, 0, 9, 1, 198, 32]]
        #--------------------------------------------------------------------
        # Создание объекта СКП уровня P7
        self.oStack = l.P7(iNpz=0x81, bTest=True)
    #enddef
    #========================================================================
    def test2(self):
        """
        Тестирование протокола P2
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P2execRq(self.lRq)
        print "Выход СКП:", out1
        print "-" * 77
        out2 = self.oStack.P2execRs(out1)
        print "Выход СКП:", out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P2execRs(self.oStack.P2execRq(Data=self.lRq)),
            self.lRq
        )
    #enddef
    #========================================================================
    def test3(self):
        """
        Тестирование протоколов P2+P3
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P3execRq(self.lRq)
        print "Выход СКП:", out1
        print "-" * 77
        out2 = self.oStack.P3execRs(Data=out1)
        print "Выход СКП:", out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P3execRs(self.oStack.P3execRq(Data=self.lRq)),
            self.lRq
        )
    #enddef
    #========================================================================
    def test4(self):
        """
        Тестирование протоколов P2+P3+P4
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P4execRq(self.lRq)
        print "Выход СКП:", out1
        print "-" * 77
        out2 = self.oStack.P4execRs(out1)
        print "Выход СКП:", out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P4execRs(self.oStack.P4execRq(Data=self.lRq)),
            self.lRq
        )
    #enddef
    #========================================================================
    def test5(self):
        """
        Тестирование протоколов P2+P3+P4+P5
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P5execRq(self.lRq)
        print "Выход СКП:", out1
        print "-" * 77
        out2 = self.oStack.P5execRs(out1)
        print "Выход СКП:", out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P5execRs(self.oStack.P5execRq(self.lRq)),
            self.lRq
        )
    #enddef
    #========================================================================
    def test6(self):
        """
        Тестирование протоколов P2+P3+P4+P5+P6
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P6execRq(self.lRq)
        print "Выход СКП:", out1
        print "=" * 77
        out2 = self.oStack.P6execRs(out1)
        print "Выход СКП:", out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P6execRs(self.oStack.P6execRq(self.lRq)),
            self.lRq
        )
    #enddef
    #========================================================================
    def test7(self):
        """
        Тестирование протоколов P2+P3+P4+P5+P6+P7
        """
        #--------------------------------------------------------------------
        '''
        # Для проверки
        out1 = self.oStack.P7execRq(self.lRq)
        print "Выход СКП: %r" % out1
        print "-" * 77
        out2 = self.oStack.P7execRs(out1)
        print "Выход СКП: %r" % out2
        print "=" * 77
        '''
        #--------------------------------------------------------------------
        self.assertEqual(
            self.oStack.P7execRs(self.oStack.P7execRq(self.lRq)),
            self.lRq
        )
    #enddef
#endclass
#############################################################################
if __name__ == '__main__':
    # Модуль запущен из CLI, а не импортирован
    #------------------------------------------------------------------------
    # Переключение текущего рабочего каталога на каталог web2py
    os.chdir("../../..")
    #------------------------------------------------------------------------
    # Добавление к списку путей поиска python-модулей пути к каталогу web2py
    sys.path.insert(0, os.getcwd())
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
    #------------------------------------------------------------------------
    # Вызов функции main() из модуля unittest.py
    unittest.main()
#endif
#############################################################################
