#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 25.02.09
# Purpose: Тестирование создания контрольной суммы LRC8 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import unittest
import os, sys
from fix_path import fix_sys_path
fix_sys_path(__file__)

if os.path.basename(os.getcwd()) == "web2py":
    # Тест запущен через import из модуля RunTest.py
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
#endif
#############################################################################
class TestAllAddrs(unittest.TestCase):
    """
    Класс для тестирования функции Lrc8() из библиотеки uLib
    """
    #========================================================================
    def test1(self):
        """
        Тест контрольной суммы LRC8 
        """
        #--------------------------------------------------------------------
        lPdu = [11, 20, 34, 57, 23]
        #--------------------------------------------------------------------
        self.assertEqual(
            l.Lrc8(lPdu),
            [11, 20, 34, 57, 23, 236]
        )
    #enddef    
    #========================================================================
    def test2(self):
        """
        Тест контрольной суммы LRC8 
        """
        #--------------------------------------------------------------------
        lPdu = [176, 8, 39, 2, 9, 92, 32]
        #--------------------------------------------------------------------
        self.assertEqual(
            l.Lrc8(lPdu),
            [176, 8, 39, 2, 9, 92, 32, 23] 
        )
    #enddef    
    #========================================================================
    def test1(self):
        """
        Тест контрольной суммы LRC8 
        """
        #--------------------------------------------------------------------
        lPdu = [2, 1, 48, 48, 48, 48, 48, 48]
        #--------------------------------------------------------------------
        self.assertEqual(
            l.Lrc8(lPdu),
            [2, 1, 48, 48, 48, 48, 48, 48, 252]
        )
    #enddef
#endclass    
#############################################################################
if __name__ == '__main__':
    # Модуль запущен из CLI, а не импортирован
    #------------------------------------------------------------------------
    # Переключение текущего рабочего каталога на каталог web2py
    os.chdir("../../..")
    #------------------------------------------------------------------------
    # Добавление к списку путей поиска python-модулей пути к каталогу web2py
    sys.path.insert(0, os.getcwd())
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
    #------------------------------------------------------------------------
    # Вызов функции main() из модуля unittest.py
    unittest.main()
#endif
#############################################################################
