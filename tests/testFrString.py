#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 01.06.09
# Purpose: Проверка преобразование каждого символа (байта) обычной строки 
# в два 16-ных символа
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import unittest
import os, sys
from fix_path import fix_sys_path
fix_sys_path(__file__)

if os.path.basename(os.getcwd()) == "web2py":
    # Тест запущен через import из модуля RunTest.py
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
#endif
#############################################################################
class TestFromString(unittest.TestCase):
    """
    Класс для тестирования функции FromString() из библиотеки uLib
    """
    #========================================================================
    def test1(self):
        """
        Преобразование каждого символа (байта) обычной строки в два 16-ных
        символа
        """
        #--------------------------------------------------------------------
        sS = "На стене 5"
        #print l.FrString(sS)
        #--------------------------------------------------------------------
        self.assertEqual(
            l.FrString(sS),
            'cd e0 20 f1 f2 e5 ed e5 20 35'
        )
    #enddef    
    #========================================================================
    def test2(self):
        """
        Преобразование каждого символа (байта) обычной строки в два 16-ных
        символа
        """
        #--------------------------------------------------------------------
        sS = "Ура! Мы ломим, гнутся шведы!"
        #print l.FrString(sS)
        #--------------------------------------------------------------------
        self.assertEqual(
            l.FrString(sS),
            'd3 f0 e0 21 20 cc fb 20 eb ee ec e8 ec 2c 20 e3 '
            'ed f3 f2 f1 ff 20 f8 e2 e5 e4 fb 21'  
        )
    #enddef
#endclass        
#############################################################################
if __name__ == '__main__':
    # Модуль запущен из CLI, а не импортирован
    #------------------------------------------------------------------------
    # Переключение текущего рабочего каталога на каталог web2py
    os.chdir("../../..")
    #------------------------------------------------------------------------
    # Добавление к списку путей поиска python-модулей пути к каталогу web2py
    sys.path.insert(0, os.getcwd())
    #------------------------------------------------------------------------
    from applications.upoweron.modules import uLib as l
    #------------------------------------------------------------------------
    # Вызов функции main() из модуля unittest.py
    unittest.main()
#endif
#############################################################################
