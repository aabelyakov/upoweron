#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 01.04.2015
# Purpose: 
'''
pkg/
  __init__.py
  components/
    core.py
    __init__.py
  tests/
    core_test.py
    __init__.py
#############################################################################
if __name__ == '__main__':
    if __package__ is None:
        import sys
        from os import path
        sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
        from components.core import GameLoopEvents
    else:
        from ..components.core import GameLoopEvents
    #endif    
'''    
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys

os.chdir("../../../..")
print os.getcwd()
# /home/aab/MyPython/web2py/applications/upoweron/modules

sys.path.append(os.getcwd())
print sys.path
'''
['/home/aab/MyPython/web2py/applications/upoweron/modules/uTest',
'/usr/local/lib/python2.7/dist-packages/GChartWrapper-0.9-py2.7.egg',
'/usr/lib/python2.7', '/usr/lib/python2.7/plat-x86_64-linux-gnu', 
'/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old',
'/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages',
'/usr/lib/python2.7/dist-packages',
'/usr/lib/python2.7/dist-packages/PILcompat', 
'/usr/lib/python2.7/dist-packages/gst-0.10', 
'/usr/lib/python2.7/dist-packages/gtk-2.0', '/usr/lib/pymodules/python2.7',
'/usr/lib/python2.7/dist-packages/ubuntu-sso-client',
'/home/aab/MyPython/web2py/applications/upoweron/modules']
'''
from applications.upoweron.modules import uLib 
print dir()
#############################################################################

#############################################################################