#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Запускать из каталога /home/aab/MyPython/web2py
#############################################################################
import os
import sys
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога на каталог web2py
os.chdir("../../..")
#print(os.getcwd())
#/home/aab/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к текущему рабочему 
# каталогу
sys.path.insert(0, os.getcwd())
#print sys.path
#----------------------------------------------------------------------------
from gluon.contrib.fpdf import FPDF
#----------------------------------------------------------------------------
# Создание объекта документа
pdf = FPDF()
#----------------------------------------------------------------------------
# Добавление DejaVu Unicode фонта (использует кодировку UTF-8)
# Фонт поддерживает более 200 языков. Смотри подробности здесь:
# http://dejavu-fonts.org/wiki/Download
# Фонты для fpdf должны лежать в папке gluon/contrib/fpdf/font
# см. Стр. 45 в файле gluon/contrib/fpdf/fpdf.py
#FPDF_FONT_DIR = os.path.join(os.path.dirname(__file__), 'font')
pdf.add_font('DejaVu', '', 'DejaVuSansMono-Bold.ttf', uni=True)
#----------------------------------------------------------------------------
# Установка размера фонта
pdf.set_font('DejaVu', '', 14)
#----------------------------------------------------------------------------
# Добавление новой pdf-страницы
pdf.add_page()
#----------------------------------------------------------------------------
# Создание ячейки с текстом
pdf.cell(40,10,'Привет, МИР!')
#----------------------------------------------------------------------------
# Установка размера фонта
pdf.set_font('DejaVu', '', 10)
#----------------------------------------------------------------------------
# Создание ячейки с текстом
pdf.cell(00, 30, 'Создано с помощью FPDF', 0, 1, 'C')
#----------------------------------------------------------------------------
# I - send the file inline to the browser. The plug-in is used if available. 
#   The name given by name is used when one selects the "Save as" option on 
#   the link generating the PDF. 
# D - send to the browser and force a file download with the name given by 
#   name. 
# F - save to a local file with the name given by name (may include a path). 
# S - return the document as a string. name is ignored.
#----------------------------------------------------------------------------
# Создание pdf-файла example.pdf в локальной ФС
pdf.output('example.pdf','F')
print "Файл 'example.pdf' сохранён в каталоге web2py"
os.system("evince ./example.pdf")
#----------------------------------------------------------------------------
#pdf.output('example.pdf','D')
#print "Файл 'example.pdf' открыт в web браузере"
#############################################################################
