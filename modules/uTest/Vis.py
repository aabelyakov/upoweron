#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 11.03.09
# Purpose: Тестирования функции Vis() для расшифровки ответа счётчика на 
# запрос "Прочитать вариант исполнения счётчика" (ВИС).
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#############################################################################
# Переключение текущего рабочего каталога на каталог web2py
os.chdir("../../../..")
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к каталогу web2py
sys.path.append(os.getcwd())
#----------------------------------------------------------------------------
from applications.upoweron.modules import uLib as l
from applications.upoweron.models.u1_db import db
from gluon.storage import Storage
#############################################################################
ss = Storage()
###########################################################################
lIn = [
   "00", "01", "02", "03", 
   "10", "11", "12", "17", 
   "20", "21", "23", "2b",
   "30", "31", "32", "33",
   "40", "41", "44", "47",
   "50", "51", "54", "57",
   "60", "62", "64", "67",
   "70", "72", "74", "77",
   "80", "82", "84", "87",
   "90", "92", "94", "97",
   "a0", "a2", "a4", "a7",       
] 
#----------------------------------------------------------------------------
for m in lIn:
   print "ШШШШШШШШШШШШШШ", m, "ШШШШШШШШШШШШШШ"
   ss.lRes = ["64", "c1", m]
   _, _, dPar = l.Vis(db, ss)
   #-------------------------------------------------------------------------
   for k, v in dPar.items():
      if v:
         print k, "=",  v
      #endif   
   #endif
#endfor   
#----------------------------------------------------------------------------
print "ШШШШШШШШШШШШШШ", m, "ШШШШШШШШШШШШШШ"
ss.lRes = ["64", "c2", m]
_, _, dPar = l.Vis(db, ss)
#----------------------------------------------------------------------------
for k, v in dPar.items():
   if v:
      print k, "=",  v
   #endif   
#endif
#############################################################################   