#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 16.12.2014
# Purpose:   
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import sys
import os
import unittest
#############################################################################
def fix_sys_path():
    """
    logic to have always the correct sys.path
    ['', PREFIX/web2py/gluon, PREFIX/web2py/site-packages, PREFIX/web2py/ ...
    """
    #========================================================================
    def add_path_first(path):
        """
        Добавить в список путей sys.path первым элементом путь path
        for p in sys.path:
            if p != path and p != (path + '/'):
                sys.path.insert(0, path)
            #enddef
        #endfor
        #--------------------------------------------------------------------
        """
        sys.path = [path] + [p for p in sys.path if (
            not p == path and not p == (path + '/'))]
    #enddeg
    #========================================================================
    path = os.path.dirname(os.path.abspath(__file__))
    #------------------------------------------------------------------------
    if not os.path.isfile(os.path.join(path, 'web2py.py')):
        i = 0
        #--------------------------------------------------------------------
        while i < 10:
            if os.path.exists(os.path.join(path,'web2py.py')):
                break
            #endif
            #----------------------------------------------------------------
            path = os.path.abspath(os.path.join(path, '..'))
            #----------------------------------------------------------------
            i += 1
        #endwhile
    #endif    
    #------------------------------------------------------------------------
    paths = [
        path,
        os.path.abspath(os.path.join(path, 'site-packages')),
        os.path.abspath(os.path.join(path, 'gluon')),
        ''
    ]
    #------------------------------------------------------------------------
    [add_path_first(path) for path in paths]
#enddef
#############################################################################
fix_sys_path()
print sys.path

