#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 06.02.09
# Purpose: Тестирование библиотечной функции Frunbus.Log()
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#############################################################################
# Переход в родительский каталог
os.chdir('..')
#----------------------------------------------------------------------------
# Вставка первым элементом списка - полного пути к родительскому каталогу
sys.path.insert(0, os.getcwd())
#############################################################################
import logging, uLib
#############################################################################
def Logger(name=None):
    """
    В log-файл попадают только те сообщения, уровень которых равен или 
    выше уровня фильтра - level в logging.basicConfig

    Уровни возможных сообщений:
    ---------------------------
    CRITICAL = 50
    ERROR    = 40
    WARNING  = 30
    INFO     = 20
    DEBUG    = 10
    NOTSET   = 0
    Например, если в logging.basicConfig установлен уровень фильтра - ERROR,
    то в log-файл будут записаны только сообщения уровней ERROR и CRITICAL  
    """
    #----------------------------------------------------------------------
    # Получение экземпляра пользовательского логгера
    logger1 = uLib.Log()
    #----------------------------------------------------------------------
    # Внимание! Русское сообщение не нужно переводить в кодировку Unicode 
    logger1.debug('Это debug-сообщение для log-файла')
    logger1.info('Это info-сообщение log-файла')
    logger1.critical('Это critical-сообщение для log-файла')
#enddef
#############################################################################
if __name__ == "__main__":
    if len(sys.argv) == 1:
        Logger()
    elif len(sys.argv) == 2:
        Logger(sys.argv[1])
    else:
        print "Ошибка CLI"
    #endif    
#endif

#############################################################################