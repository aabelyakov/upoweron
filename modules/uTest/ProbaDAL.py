#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 08.06.10
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
from model import db 
#############################################################################
'''
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Вызов программы probaDAL.py

python web2py.py -S welcome -M model.py -R probaDAL.py
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"req_grp",
SQLField("cod", "string", length=2),
SQLField("naim", "string", length=256),

"req",  
SQLField("req_grp_id", db.req_grp),
SQLField("zapr", "string", length=8),
SQLField("naim", "string", length=256),
'''
rows = db(
    (db.req_grp.id==db.req.req_grp_id) & \
    (db.req.id==db.req_prm.req_id )
    ).select()
print rows[0]

for r in rows: 
    print r.req_grp.cod, r.req_grp.naim.decode("utf-8").encode("cp866")
    print r.req.zapr, r.req.naim.decode("utf-8").encode("cp866")
#endfor    
