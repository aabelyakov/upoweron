#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 13.01.10
# Purpose: Использование дескритора класса для получения логгера
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#############################################################################
# Переход в родительский каталог
os.chdir('..')
#----------------------------------------------------------------------------
# Вставка первым элементом списка - полного пути к родительскому каталогу
sys.path.insert(0, os.getcwd())
#############################################################################
import logging, uLib   
#############################################################################
def _get_instance_name(instance):
    modulename = instance.__class__.__module__
    #print(modulename)
    classname = instance.__class__.__name__ 
    #print(classname)
    instid = hex(id(instance))[-2:]
    #print(instid)
    ret = "%s.%s.0x%s" % (modulename, classname, instid)
    return ret
#enddef
#############################################################################
class class_logger(object):
    '''
    Получение логгера класса через дескриптор 
    '''
    #========================================================================
    def __init__(self):
        self._logger = None 
    #enddef
    #========================================================================
    def __get__(self, instance, owner):
        if not self._logger:
            self._logger = logging.getLogger(
                "%s.%s" % (owner.__module__, owner.__name__)
            )
        #endif
        #--------------------------------------------------------------------
        return self._logger   
    #enddef
#endclass
#############################################################################
class instance_logger(object):
    '''
    Получение логгера объекта класса через дескриптор 
    '''
    #========================================================================
    def __init__(self):
        self._logger = None             
    #enddef
    #========================================================================
    def __get__(self, instance, owner):
        if not self._logger:
            #self._logger = logging.getLogger(_get_instance_name(instance))
            # Фрейм исполнения по состоянию на один шаг назад
            frame = sys._getframe(1)
            #----------------------------------------------------------------
            # Имя метода класса
            #global MethodName
            MethodName = frame.f_code.co_name + ":" 
            self._logger = logging.getLogger(MethodName)
        #endif    
        return self._logger 
    #enddef
#endclass    
#############################################################################
if __name__ == "__main__":
#------------------------------------------------------------------------
    logging.basicConfig(
        filename="MyLog.txt", 
        level=logging.NOTSET,
        format="%(asctime)s,%(msecs)03d: %(levelname)s: %(name)s: %(message)s",
        datefmt='%H:%M:%S',
        filemode="w"
    )
    #------------------------------------------------------------------------
    # Описание класса, в котором логгеры берутся через дескрипторы
    class MyCls():
        ClsLog = class_logger()
        InstLog = instance_logger()
        def a1(self):
            print("a1")
            self.InstLog.info("Привет от a1")
            self.ClsLog.info("Привет от класса MyCls")
        #enddef    
    #endclass
    #------------------------------------------------------------------------
    # Создание объекта Ob класса MyCls
    Ob = MyCls()
    Ob.a1()
    Ob1 = MyCls()
    #------------------------------------------------------------------------
    # Запись данных в лог-файл "MyLog.txt"
    """
    MyCls.ClsLog.info("Мой класс")
    Ob.InstLog.info("Мой экземпляр")
    Ob1.InstLog.info("Мой экземпляр")
    
    try:
        1/0
    except ZeroDivisionError:    
        Ob.InstLog.exception("Исключение")
        raise
    #endtry    
    """
#############################################################################
   