#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 27.05.09
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#############################################################################
# Текущий рабочий каталог - родительский каталог
os.chdir('..')
#----------------------------------------------------------------------------
# Вставка первым элементом списка - полного пути к родительскому каталогу
sys.path.insert(0, os.getcwd())
#############################################################################
import uLib
#############################################################################
sSql = """
SELECT 
    request,
    len_res,
    naim_type,
    naim,
    naim_par,
    yb,
    ye,
    ib,
    ie,
    type_par,
    param
FROM
    types
    INNER JOIN responses ON (responses.types_id = types.id)
    INNER JOIN requests ON (responses.requests_id = requests.id)
    INNER JOIN po ON (responses.po_id = po.id)
    WHERE
    request = ?"""

print sSql
print 70 * "-"
print uLib.ClearSql(sSql)

###########################################################################