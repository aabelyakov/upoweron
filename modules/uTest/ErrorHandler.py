# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 12.01.2010
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import logging, sys 
from errorhandler import ErrorHandler
#############################################################################
def testErrorHandler():
    # Конфигурирование логгера
    logging.basicConfig(
        level=logging.NOTSET, 
        stream = sys.stdout,
        #stream = sys.stderr,
        #filename = "MyLog.txt", 
        filemode='w'
    )
    #------------------------------------------------------------------------
    # Создание экземпляра логгера
    logger = logging.getLogger(name="КЛАСС")
    #------------------------------------------------------------------------
    e = ErrorHandler()
    print logger
    # <logging.RootLogger instance at 0x00E34C10> 
    print e
    # <errorhandler.ErrorHandler instance at 0x00E3B378>
    #------------------------------------------------------------------------
    print e.fired
    # False
    #------------------------------------------------------------------------
    logger.info('Сообщение типа info %s', "для меня")
    print e.fired
    # False
    #------------------------------------------------------------------------
    logger.error('Возникла %s', "ошибка!")
    print e.fired
    #------------------------------------------------------------------------
    if e.fired:
        e.reset()
    #endif    
    #------------------------------------------------------------------------
    print e.fired
    # False
    #------------------------------------------------------------------------
    try:
        1/0
    except ZeroDivisionError:    
        logger.exception("Возникла исключительная ситуация")
    #endtry 
    """
    ERROR:root: Возникла исключительная ситуация
    Traceback (most recent call last):
      File "D:\MyProg\_MyPython\PowerOn\Test\testErrorHandler.py", line 45,
    in testErrorHandler
        1/0
    ZeroDivisionError: integer division or modulo by zero
    """
    #------------------------------------------------------------------------
    print e.fired
    # True
#enddef    
#############################################################################
if __name__ == "__main__":
    testErrorHandler()