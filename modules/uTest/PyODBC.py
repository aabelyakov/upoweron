#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Purpose: Эта программа была реализована для MsSQL
# Необходимо внести в неё изменения для PgSQL
# Created: 16.04.2008
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
from pprint import pprint
import locale, sys, pyodbc
#============================================================================
# Установка русской культурной среды (Locale) для pprint
print locale.setlocale(locale.LC_ALL, '')
# Russian_Russia.1251
#============================================================================
print "========== pyodbc.dataSources()"
# Возвращает словарь, отображающий доступные источники данных (DSN)
# с их описанием 
ss = pyodbc.dataSources()

for s in ss.items():
    key, value = s
    print unicode(key, "cp1251"), "-->", unicode(value, "cp1251")

"""
PostgreSQL35W --> PostgreSQL Unicode
LocalServer --> SQL Server
MySqlDsn --> MySQL ODBC 5.1 Driver
Visio Database Samples --> Microsoft Access Driver (*.mdb)
MyAccess --> Microsoft Access Driver (*.mdb)
msSQL --> SQL Server
MyAccess5 --> Microsoft Access Driver (*.mdb)
MyAccess4 --> Microsoft Access Driver (*.mdb)
MyAccess1 --> Microsoft Access Driver (*.mdb)
MyAccess0 --> Microsoft Access Driver (*.mdb)
MyAccess3 --> Microsoft Access Driver (*.mdb)
MyAccess2 --> Microsoft Access Driver (*.mdb)
TestDB --> PostgreSQL Unicode
Excel Files --> Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)
dBASE Files --> Microsoft Access dBASE Driver (*.dbf, *.ndx, *.mdx)
PgServer --> PostgreSQL Unicode
MS Access Database --> Microsoft Access Driver (*.mdb, *.accdb)
"""
#============================================================================
print "========== pyodbc.connect()"
'''
# Соединение без предварительного создания пользовательского DSN (РАБОТА)
con = pyodbc.connect("DRIVER={SQL Server};SERVER=zif-srv-4;" + \
                     "DATABASE=Northwind", autocommit=True)

#============================================================================
# Соединение с предварительным созданием пользовательского DSN (РАБОТА)
con = pyodbc.connect("DSN=msSQL;HOST=zif-srv-4;db=Northwind", 
                     autocommit=True)
'''                    
#============================================================================
# Создание соединения без использования пользовательского DSN (ДОМ)
con = pyodbc.connect("DRIVER={PostgreSQL Unicode};SERVER=localhost;" + \
                     "DATABASE=power;UID=postgres;PWD=cj.pybr", 
                     autocommit=True)
''' 
#============================================================================

#Соединение с предварительным созданием пользовательского DSN (ДОМ)
con = pyodbc.connect("DSN=msSQL;HOST=(localhost);db=Northwind;", 
                     autocommit=True)
'''                   
#============================================================================
print con
# <pyodbc.Connection object at 0x015CDD00>

#=================== Атрибуты и методы соединения con =======================
print dir(con)
"""
['__class__', '__delattr__', '__doc__', '__getattribute__', '__hash__',
'__init__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', 
'__setattr__', '__str__', 'autocommit', 'close', 'commit', 'cursor', 
'execute', 'getinfo', 'rollback', 'searchescape', 'timeout']
"""
#============================================================================
print "========== connection.autocommit"
# Атрибут autocommit=True -  после логического завершения транзакции
# автоматически записывает программные буферы в БД (на диск)
print con.autocommit
# True
#============================================================================
print "========== connection.commit()"
# Метод сommit() - при выполнении этого метода транзакция принудительно
# завершается и программные буферы записываются в БД (на диск)
print con.commit()
# None
#============================================================================
print "========== connection.execute()"
# Метод execute() - выполнение запроса к таблице БД, cоздание нового объекта
# курсора и его заполнение 
cursor = con.execute("select * from params;")

print type(cursor)
# <type 'pyodbc.Cursor'>

for r in cursor:
    print r[0], r[1], r[2]

# 0306050086 02 None

print type(r)
# <type 'pyodbc.Row'>
#============================================================================
print "========== connection.getinfo()"
# Метод getinfo() - вызов функции SQLGetInfo() с константами, 
# определенными в модуле PyODBC
print con.getinfo(pyodbc.SQL_DATA_SOURCE_NAME)
# msSQL

print con.getinfo(pyodbc.SQL_DATABASE_NAME)
# Northwind

print con.getinfo(pyodbc.SQL_COLLATION_SEQ)
# Code Page 1251

print con.getinfo(pyodbc.SQL_DRIVER_NAME)
# SQLSRV32.DLL

print con.getinfo(pyodbc.SQL_DRIVER_ODBC_VER)
# 03.52

print con.getinfo(pyodbc.SQL_KEYWORDS)
"""
BREAK,BROWSE,BULK,CHECKPOINT,CLUSTERED,COMMITTED,COMPUTE,CONFIRM,CONTROLROW,
DATABASE,DBCC,DISK,DISTRIBUTED,DUMMY,DUMP,ERRLVL,ERROREXIT,EXIT,FILE,
FILLFACTOR,FLOPPY,HOLDLOCK,IDENTITY_INSERT,IDENTITYCOL,IF,KILL,LINENO,LOAD,
MIRROREXIT,NONCLUSTERED,OFF,OFFSETS,ONCE,OVER,PERCENT,PERM,PERMANENT,PLAN,
PRINT,PROC,PROCESSEXIT,RAISERROR,READ,READTEXT,RECONFIGURE,REPEATABLE,RETURN,
ROWCOUNT,RULE,SAVE,SERIALIZABLE,SETUSER,SHUTDOWN,STATISTICS,TAPE,TEMP,
TEXTSIZE,TOP,TRAN,TRIGGER,TRUNCATE,TSEQUEL,UNCOMMITTED,UPDATETEXT,USE,
WAITFOR,WHILE,WRITETEXT"""

print con.getinfo(pyodbc.SQL_SEARCH_PATTERN_ESCAPE)
# \
#============================================================================
print "========== connection.cursor()"
# Метод cursor() - cоздание нового объекта курсора (пустого)
curs = con.cursor()

print type(curs)
# <type 'pyodbc.Cursor'>
#===================== Атрибуты и методы курсора ============================
print dir(curs)
"""
['__class__', '__delattr__', '__doc__', '__getattribute__', '__hash__', 
'__init__', '__iter__', '__new__', '__reduce__', '__reduce_ex__',
 '__repr__', '__setattr__', '__str__', 'arraysize', 'close', 'columns', 
 'connection', 'description', 'execute', 'executemany', 'fetchall', 
 'fetchmany', 'fetchone', 'foreignKeys', 'getTypeInfo', 'next', 'nextset', 
 'primaryKeys', 'procedureColumns', 'procedures', 'rowIdColumns', 
 'rowVerColumns', 'rowcount', 'setinputsizes', 'setoutputsize', 
 'statistics', 'tables']"""
#============================================================================
print "========== cursor.tables()"
# Метод tables() -  вызов системной процедуры sp_tables для
# вывода списка с именами таблиц, входящих в БД
for row in curs.tables():
    print row.table_name
"""
============================= Системные таблицы =============================
dtproperties
syscolumns
syscomments
sysdepends
sysfilegroups
sysfiles
sysfiles1
sysforeignkeys
sysfulltextcatalogs
sysfulltextnotify
sysindexes
sysindexkeys
sysmembers
sysobjects
syspermissions
sysproperties
sysprotects
sysreferences
systypes
sysusers
sysconstraints
syssegments
========================= Пользовательские таблицы ==========================
Categories
CustomerCustomerDemo
CustomerDemographics
Customers
Employees
EmployeeTerritories
Order Details
Orders
Products
Region
Shippers
Suppliers
Territories
Alphabetical list of products
Category Sales for 1997
Current Product List
Customer and Suppliers by City
Invoices
Order Details Extended
Order Subtotals
Orders Qry
Product Sales for 1997
Products Above Average Price
Products by Category
Quarterly Orders
Sales by Category
Sales Totals by Amount
Summary of Sales by Quarter
Summary of Sales by Year
"""
#============================================================================
print "========== cursor.columns()"
'''
Метод columns(table=None, catalog=None, schema=None, column=None)
вызов ODBC функции SQLColumns для вывода информации о колонках таблиц БД
Каждая строка курсора содержит следующие поля:  
================= Для PgSQL ==========================
00-table_cat        - квалификатор таблицы (имя БД)
01-table_schem      - имя собственника таблицы
02-table_name       - имя таблицы
03-column_name      - имя поля (колонки) таблицы
04-data_type        - номер типа данных, хранящихся в колонке
05-type_name        - имя типа данных
06-column_size      - длина поля или число значащих разрядов
07-buffer_length    - длина буфера
08-decimal_digits   - количество десятичных цифр после точки
09-num_prec_radix   - основание системы счисления для числовых типов 
10-nullable         - наличие NULL значений в колонке (1-разрешены; 0-запрещены)
11-remarks          - всегда NULL
12-column_def       - значение по умолчанию для колонки
13-sql_data_type    - аналогична DATA_TYPE, исключая типы datetime и interval
14-sql_datetime_sub - номер типов для datetime и interval (для других-NULL)
15-char_octet_length- максимальная длина для символьных и целых полей
                      (для других типов данных - NULL) 
16-ordinal_position - порядковый номер колонки в таблице (отсчет колонок с 1)
17-is_nullable: Одно из: SQL_NULLABLE, SQL_NO_NULLS, SQL_NULLS_UNKNOWN. 

'''    
column_names = []
for col in curs.columns("params"):
    print col.column_name
    column_names.append(col.column_name)
#endfor
print column_names    
#============================================================================
print "======= curs.rowIdColumns()"
'''
rowIdColumns(table, catalog=None, schema=None, nullable=True)

Вызывает функцию SQLSpecialColumns c SQL_BEST_ROWID и создаёт 
результирующий набор колонок, которые уникально идентифицируют строку.
Возвращает объект курсора. 

Каждая строка результирующего набора строк содержит
следующие поля:
00-scope: One of SQL_SCOPE_CURROW, SQL_SCOPE_TRANSACTION, or SQL_SCOPE_SESSION 
01-column_name 
02-data_type: The ODBC SQL data type constant (e.g. SQL_CHAR) 
03-type_name 
04-column_size 
05-buffer_length 
06-decimal_digits 
07-pseudo_column: One of SQL_PC_UNKNOWN, SQL_PC_NOT_PSEUDO, SQL_PC_PSEUDO 
'''

for st in curs.rowIdColumns("params"):
    print st
#endfor

#(2, u'oid', 4, u'oid', 10, 4, 0, 2)
#============================================================================
print "======= cursor.statistics()" 
# Метод statistics() - вызов системной процедуры sp_statistics 
# для вывода списка всех индексов и статистики по указанным таблицам 
# или индексированным представлениям (view)
for st in curs.statistics("Territories"):
    print st
"""
0) TABLE_QUALIFIER - квалификатор таблицы (имя БД)
1) TABLE_OWNER - имя собственника таблицы
2) TABLE_NAME - имя таблицы
3) NON_UNIQUE - 0 = Unique 1 = Not unique. NOT NULL
4) INDEX_QUALIFIER - имя собственника индекса. Совпадает с TABLE_NAME
5) INDEX_NAME - Имя индеса. Всегда возвращает значение
6) TYPE - Всегда возвращает значение. 
-- 0 = Статистика для таблицы, 
-- 1 = Кластеризованный
-- 2 = Хешированный
-- 3 = Другой
7) SEQ_IN_INDEX - позиция колонок таблицы внутри индекса
8) COLUMN_NAME - Имя каждой колонки таблицы TABLE_NAME. Всегда возвращает 
значение
9) COLLATION - Порядок сравнения. SQL server всегда возвращает А.
-- A = по возрастанию
-- D = по убыванию
-- NULL = нет порядка сравнения
10) CARDINALITY - число строк в таблице или уникальных значений в индексе
11) PAGES - число страниц для запоминания в индексе или таблице
12) FILTER_CONDITION - всегда None

(u'Northwind', u'dbo', u'Territories', 0, u'Territories', u'PK_Territories', 
3, 1, u'TerritoryID', 'A', None, None, None)
""" 
#============================================================================
print "======= cursor.execute()"
# Исполнение запроса или команды SQL 
# Метод execute(sql [,parameters]) --> cursor.object or count, где:
# sql - текст запроса, 
# parameters - параметры запроса
# возвращает наполненный данными объект запроса или значение счетчика
# успешно измененных записей в БД для команд insert, update и delete 

# стандартный вид
#       cursor.execute("select a from tbl where b=? and c=?", (x, y))
# pyodbc - расширение
#       cursor.execute("select a from tbl where b=? and c=?", x, y)
#a=2
a = "0306050086"
b="Santa%"

#cursobj = curs.execute("select * from Territories where RegionID=?" + \
#                       " and TerritoryDescription like ?", a, b)
cursobj = curs.execute("select * from params where ser_num=?" , a)

for row in cursobj:
    print row
"""
(u'90405', u'Santa Monica                                      ', 2)
(u'95054', u'Santa Clara                                       ', 2)
(u'95060', u'Santa Cruz                                        ', 2)
"""

print type(row)
# <type 'pyodbc.Row'>    


# Внимание!!! После цикла по cursobj он становится пустым!!!!!!
# Поэтому надо повторить запрос
#cursobj = curs.execute("select * from Territories where RegionID=?" + \
#                       " and TerritoryDescription like ?", a, b)
cursobj = curs.execute("select * from params where ser_num=?" , a)

rs = cursobj.fetchall()

print type(rs)
# <type 'list'>

print rs
"""
[(u'90405', u'Santa Monica                                     ', 2), 
(u'95054', u'Santa Clara                                       ', 2), 
(u'95060', u'Santa Cruz                                        ', 2)]
"""
for r in rs:
    print r
"""
(u'90405', u'Santa Monica                                      ', 2)
(u'95054', u'Santa Clara                                       ', 2)
(u'95060', u'Santa Cruz                                        ', 2)
(u'90405', u'Santa Monica                                      ', 2)
(u'95054', u'Santa Clara                                       ', 2)
(u'95060', u'Santa Cruz                                        ', 2)
"""

#a = 2
b = 'Santa%'
#rows = curs.execute("select * from Territories where RegionID=?" + \
#                    " and TerritoryDescription like ?", a, b).fetchall()
rows = curs.execute("select * from params where ser_num=?" , a)

for row in rows:
    print row
"""
(u'90405', u'Santa Monica                                      ', 2)
(u'95054', u'Santa Clara                                       ', 2)
(u'95060', u'Santa Cruz                                        ', 2) 
"""
#============================================================================
print "========== cursor.description"
# Атрибут description - вывод атрибутов колонок таблиц, участвующих в запросе
print curs.description
"""
Если был запрос curs.execute() и курсор не пустой
Выводятся реквизиты столбцов

name, type_code, display_size, internal_size, precision, scale, null_ok
=============================================================================
(('TerritoryID', <type 'unicode'>, None, 20, None, None, False), 
('TerritoryDescription', <type 'unicode'>, None, 50, None, None, False), 
('RegionID', <type 'int'>, None, 10, None, None, False))

Если не было запроса curs.execute() и курсор пустой
Выводится статистика
(('table_cat', <type 'unicode'>, None, 128, None, None, True), 
('table_schem', <type 'unicode'>, None, 128, None, None, True), 
('table_name', <type 'unicode'>, None, 128, None, None, False), 
('non_unique', <type 'int'>, None, 5, None, None, True), 
('index_qualifier', <type 'unicode'>, None, 128, None, None, True), 
('index_name', <type 'unicode'>, None, 128, None, None, True), 
('type', <type 'int'>, None, 5, None, None, False), 
('ordinal_position', <type 'int'>, None, 5, None, None, True), 
('column_name', <type 'unicode'>, None, 128, None, None, True), 
('asc_or_desc', <type 'str'>, None, 1, None, None, True), 
('cardinality', <type 'int'>, None, 10, None, None, True), 
('pages', <type 'int'>, None, 10, None, None, True), 
('filter_condition', <type 'str'>, None, 128, None, None, True))
"""
#============================================================================
print "========== cursor.primaryKeys()"
# Метод primaryKeys() - вызов системной процедуры sp_pkeys для вывода кортежа
# с информацией о первичных ключах одной таблицы в текущем окружении
for pk in curs.primaryKeys("Territories"):
    print pk
    
"""
TABLE_QUALIFIER - Имя квалификатора (имя БД) таблицы с первичным ключом. 
Может возвращать значение NULL
TABLE_OWNER - Имя собственника таблицы. Всегда возвращает значение
TABLE_NAME - Имя таблицы. В SQL Server эта колонка представляет имя таблицы 
такое, указанное в таблице системных колонок. Всегда возвращает значение
COLUMN_NAME - Имя колонки, для каждой колонки приводится имя её таблицы. 
В SQL Server эта колонка представляет имя таблицы такое, указанное в таблице 
системных колонок. Всегда возвращает значение
KEY_SEQ - Последовательный номер колонки в многоколоночном первичном ключе.  
PK_NAME - Идентификатор первичного ключа или NULL, если он не пригоден для 
источника данных.

(u'Northwind', u'dbo', u'Territories', u'TerritoryID', 1, u'PK_Territories')
"""
#============================================================================
print "========== cursor.foreignKeys()"
# Метод foreignKeys() - вызов системной процедуры sp_fkeys для вывода 
# кортежа с информацией о внешних ключах одной таблицы в текущем окружении
for fk in curs.foreignKeys("Territories"):
    print fk
    
"""
PKTABLE_QUALIFIER - Имя квалификатора (имя БД) таблицы с первичным ключом. 
Может возвращать значение NULL
PKTABLE_OWNER - Имя собственника таблицы с первичным ключом. 
PKTABLE_NAME - Имя таблицы с первичным ключом. Всегда возвращает значение
PKCOLUMN_NAME - Имя колонки (колонок) с первичным ключом, для каждой 
колонки  - имя её таблицы (таблиц). Всегда возвращает значение
FKTABLE_QUALIFIER - Имя квалификатора таблицы (имя БД) с внешним ключом. 
Может возвращать значение NULL. 
FKTABLE_OWNER - Имя собственника таблицы с внешним ключом. Всегда возвращает 
значение
FKTABLE_NAME - Имя таблицы с внешним ключом. Всегда возвращает значение
FKCOLUMN_NAME - Имя колонки (колонок) с внешним ключом, для каждой колонки -
имя её таблицы (таблиц). Всегда возвращает значение
KEY_SEQ - Последовательный номер колонки в многоколоночном первичном ключе. 
Всегда возвращает значение 
UPDATE_RULE - Действие, применяемое к внешнему ключу, когда выпоняется 
операция обновления.  SQL Server возвращает 0 или 1 для этих колонок. 
Open Data Services шлюзы могут возвращать 0, 1, or 2: 
-- 0=каскадное изменение внешнего ключа.
-- 2=установить внешний ключ в NULL
DELETE_RULE - Действие, применяемое к внешнему ключу, когда выпоняется 
операция удаления. SQL Server возвращает 0 или 1 для этих колонок. Open 
Data Services шлюзы могут возвращать 0, 1, or 2: 
-- 0=каскадное изменение внешнего ключа.
-- 1=не изменять внешний ключ, если он имеется.
-- 2=установить внешний ключ в NULL
FK_NAME - Идентификатор внешнего ключа или NULL, если он не пригоден для 
источника данных. SQL Server возвращает имя ограничения FOREIGN KEY.   
PK_NAME - Идентификатор первичного ключа или NULL, если он не пригоден для 
источника данных. SQL Server возвращает имя ограничения PRIMARY KEY. 

(u'Northwind', u'dbo', u'Territories', u'TerritoryID', u'Northwind', u'dbo', 
u'EmployeeTerritories', u'TerritoryID', 1, 1, 1, 
u'FK_EmployeeTerritories_Territories', u'PK_Territories', 7)"""
#============================================================================
# Закрытие курсора и соединения
curs.close()
con.close()
#============================================================================




