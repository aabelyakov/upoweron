#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 16.11.09
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
from comtypes.client import CreateObject
#import comtypes.test
#############################################################################
xlRangeValueDefault = 10

#print dir(comtypes)
ie = CreateObject("InternetExplorer.Application")
print ie.Visible
#False
ie.Visible = True
#############################################################################
xl = CreateObject("Excel.Application")
xl.Workbooks.Add()
xl.Range["A1", "C1"].Value[xlRangeValueDefault] = (10,"20",31.4)
print xl.Range["A1", "C1"].Value[xlRangeValueDefault]
# ((10.0, 20.0, 31.399999999999999),)
xl.Visible = True
