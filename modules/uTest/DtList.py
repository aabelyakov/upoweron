#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 28.12.2014
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#----------------------------------------------------------------------------
# Текущий рабочий каталог - это каталог web2py
os.chdir('../../../..')
#print os.getcwd()
# /home/aab/MyPython/web2py
#----------------------------------------------------------------------------
# Включение в список путей импорта sys.path - пути к каталогу web2py
sys.path.insert(0, os.getcwd())
#----------------------------------------------------------------------------
from applications.upoweron.modules import uLib as l
from applications.upoweron.models.u1_db import db
#############################################################################
# Создание хранилища для псевдо-сессии
ss = l.PseudoSs()
#----------------------------------------------------------------------------
# Дата начала опроса
ss.xCp.sDtBg = "2015.05.01"
#----------------------------------------------------------------------------
# Дата окончания опроса
ss.xCp.sDtEn = "2015.05.02"
#----------------------------------------------------------------------------
def spis():
    for dt in l.DtList(db, ss):
        print dt
    #endfor    
#enddef
#----------------------------------------------------------------------------
# Десятичное число запрошенных байтов (24байт/час * 1час = 24байт)
ss.xCp.B = 24
spis()
'''
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=-1)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=1, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=2, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=3, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=4, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=5, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=7, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=8, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=9, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=10, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=11, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=13, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=14, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=15, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=16, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=17, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=19, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=20, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=21, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=22, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=23, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=1, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=2, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=3, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=4, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=5, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=7, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=8, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=9, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=10, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=11, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=13, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=14, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=15, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=16, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=17, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=19, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=20, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=21, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=22, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=23, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
'''
#----------------------------------------------------------------------------
print "=" * 77
# Десятичное число запрошенных байтов (24байт/час * 2час = 48байт)
ss.xCp.B = 48
spis()
'''
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=-1)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=2, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=4, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=8, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=10, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=14, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=16, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=20, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=22, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=2, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=4, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=8, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=10, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=14, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=16, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=20, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=22, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
'''
#----------------------------------------------------------------------------
print "=" * 77
# Десятичное число запрошенных байтов (24байт/час * 3час = 72байт)
ss.xCp.B = 72
spis()
'''
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=-1)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=3, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=9, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=15, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=1, tm_hour=21, tm_min=0, tm_sec=0, tm_wday=4, tm_yday=121, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=3, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=6, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=9, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=12, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=15, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=18, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
time.struct_time(tm_year=2015, tm_mon=5, tm_mday=2, tm_hour=21, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=122, tm_isdst=0)
'''
#############################################################################
