#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 26.01.10
# Purpose: Medusa.py
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import asyncore
from medusa import http_server
#############################################################################
server=http_server.http_server(ip='localhost', port=8080)
asyncore.loop()
#############################################################################