#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 18.09.2014
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################

val = int("0b01000001", 2)
#--------------------------------------------------------
dPar = {}
#--------------------------------------------------------
# Выделение двух старших разрядов из байта текущего 
# тарифа
#val1 = val & 0b11
#--------------------------------------------------------
if val & 0b01000000 == 0b01000000:
    # Бит0 = 0, бит1 = 1 - до превышения лимита энергии 
    # по текущему тарифу за расчетный период
    #----------------------------------------------------
    dPar["lim_tt"] = "0"
    #----------------------------------------------------
elif val & 0b10000000 == 0b10000000:
    # Бит0 = 1, бит1 = 0 - после превышения лимита 
    # энергии по текущему тарифу за расчетный период
    #----------------------------------------------------
    dPar["lim_tt"] = "1"
    #----------------------------------------------------
else:
    # Неверный код в старшем полубайте номера текущего 
    # тарифа
    #----------------------------------------------------
    Info(u"Неверный код в старшем полубайте номера "
         u"текущего тарифа") 
#endif
#--------------------------------------------------------
# Обнуление старших 0-го и 1-го битов байта
# Для счетчиков СЭБ-1ТМ.02:
# бит 0 = 0, бит 1 = 1 – до превышения лимита энергии 
# за расчетный период;
# бит 0 = 1, бит 1 = 0 – после превышения лимита энергии
# по тарифам за расчетный период
val &= 0x3f
#--------------------------------------------------------
# 00 - это 1-й тариф, 07 - это 8-ой тариф
val += 1
#--------------------------------------------------------
dPar["d_tt"] = val
#--------------------------------------------------------
print dPar