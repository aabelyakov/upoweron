#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 22.09.09
# Purpose: Тестирование модуля argparse.py
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import argparse, sys
#############################################################################
def myfunc():
    #------------------------------------------------------------------------
    pass
#enddef
#############################################################################
if __name__ == "__main__":
    # Модуль testArgParse вызван, а не импортирован
    #------------------------------------------------------------------------
    # Создание экземпляра класса ArgumentParser
    parser = argparse.ArgumentParser(
        # Строка с кратким описанием программы 
        description = u'Сумма целых чисел, принятых из CLI',
        #--------------------------------------------------------------------
        # Имя программы для строки usage:
        prog = u"MyProg.py",
    )
    #------------------------------------------------------------------------
    # Добавление описания первого аргумента
    parser.add_argument(
        # Целые числа
        "integers",                
        #--------------------------------------------------------------------
        # Описание аргумента - "int"
        metavar = "int", 
        #--------------------------------------------------------------------
        # Тип каждого аргумента
        type = int,  
        #--------------------------------------------------------------------
        # Список целых чисел из диапазона 0...9
        choices = xrange(10),
        #--------------------------------------------------------------------
        # Один или более аргументов обязательны
        nargs = "+",               
        #--------------------------------------------------------------------
        # Строка помощи по этому аргументу
        help = "Целые числа из диапазона 0..9" 
    )    
    #------------------------------------------------------------------------
    # Добавление описания второго аргумента
    parser.add_argument(
        # Имя параметра
        "--sum", 
        #--------------------------------------------------------------------
        # Имя метода
        dest = "accumulate", 
        #--------------------------------------------------------------------
        # Имя действия
        action = "store_const",
        #--------------------------------------------------------------------
        # Имя функции, производимой над набором второго аргумента 
        const = sum,
        #--------------------------------------------------------------------
        # Значение по умолчанию
        default = max, 
        #--------------------------------------------------------------------
        # Строка помощи по этому аргументу
        help = "Сумма целых чисел (По умолчанию: найти максимальное число)" 
    )
    #------------------------------------------------------------------------
    # Разбор аргументов командной строки
    args = parser.parse_args()
    #------------------------------------------------------------------------
    # Вывод в консоль значений аргуметров типа int
    print args.accumulate(args.integers)
#endif
#############################################################################
'''
MyProg.py -h
============

usage: MyProg.py [--sum] int [int ...]

Sum the integers at the command line

positional arguments:
  int         an integer in the range 0..9

optional arguments:
  -h, --help  show this help message and exit
  --sum       sum the integers (default: find the max)
'''