#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 30.11.2012
# Purpose: Восстановление связи таблиц rqs и prm по условию 
# (prm.rqs_id==rqs.id) через связь по условию (rqs.zapr==prm.zapr) после 
# внесения изменений в обе таблицы 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#############################################################################
import os
import sys
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('/home/aab/MyPython/web2py')
#print(os.getcwd())
#/home/aabelyakov/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление к списку путей поиска python-модулей пути к текущему рабочему 
# каталогу
sys.path.append(os.getcwd())
#print(sys.path)
#----------------------------------------------------------------------------
from applications.upoweron.modules import gLib as l
from applications.upoweron.models.u1_db import db
#############################################################################
'''
Структура таблицы rqs
~~~~~~~~~~~~~~~~~~~~~
rqs.id, rqs.prf, rqs.zapr, rqs.op, rqs.znaim
#----------------------------------------------------------------------------
Структура таблицы prm
~~~~~~~~~~~~~~~~~~~~~
prm.id, prm.rqs_id, prm.zapr, prm.naim, prm.yb, prm.ye, prm.typ, prm.col
'''
#----------------------------------------------------------------------------
# Выборка всех записей из таблицы rqs 
oRows = db().select(
    db.rqs.ALL,
    orderby = db.rqs.prf | db.rqs.zapr 
)  
#----------------------------------------------------------------------------
# Чистка таблицы rqs
TruncTable(db, "rqs")
#----------------------------------------------------------------------------
# Запись отсортированных данных в таблицу rqs
for r in oRows:
    print r.id, r.prf, r.zapr, r.op, r.znaim
    #------------------------------------------------------------------------
    # Формирование таблицы rqs
    db.rqs.insert(
        # rqs.id - формируется автоматически при вставке новой записи
        prf=r.prf,
        zapr=r.zapr, 
        op=r.op, 
        znaim=r.znaim
    )
#endfor  
#----------------------------------------------------------------------------
# Сброс буферов на диск
db.commit()
#----------------------------------------------------------------------------
for row in oRows:
    # Таблицы rqs и prm связаны по полю zapr
    # Обновление поля rqs_id таблицы prm по полю rqs.id
    #print row.id, row.zapr
    db(db.prm.zapr==row.zapr).update(rqs_id=row.id)
#endfor  
#----------------------------------------------------------------------------
# Сброс буферов на диск
db.commit()
#----------------------------------------------------------------------------
# Выборка всех записей из таблицы prm
oRows = db().select(
    db.prm.ALL,
    orderby = db.prm.zapr | db.prm.yb
)  
#----------------------------------------------------------------------------
# Чистка таблицы prm
TruncTable(db, "prm")
#----------------------------------------------------------------------------
# Запись отсортированных данных в таблицу prm
for r in oRows:
    print r.id, r.rqs_id, r.zapr, r.naim, r.yb, r.ye, r.typ, r.col
    #------------------------------------------------------------------------
    # Формирование таблицы rqs
    db.prm.insert(
        # prm.id - формируется автоматически при вставке новой записи
        rqs_id = r.rqs_id,
        zapr = r.zapr, 
        naim = r.naim,
        yb = r.yb,
        ye = r.ye,
        typ = r.typ,
        col = r.col
    )
#endfor
#----------------------------------------------------------------------------
# Сброс буферов на диск
db.commit()
#----------------------------------------------------------------------------
print "Восстановление связи таблиц rqs и prm по условию "\
      "(prm.rqs_id==rqs.id) завершено"
#############################################################################