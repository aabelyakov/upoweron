#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 04.12.09
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
from getopt import getopt
import sys, os
#############################################################################
def Copyright():
    print "\n(с) Беляков А.А. 2008" 
#enddef
#############################################################################
def Main(ar):
    try:
        opts, args = getopt(
            ar, 
            "cho:r", 
            ["help", "output=", "copyright", "revision"]
        )
    except GetoptError:
        ei = sys.exc_info()
        print '%s\n%s' % (ei[0], ei[1][1])
        #--------------------------------------------------------------------
        # Напечатать страничку помощи
        Usage()
        #--------------------------------------------------------------------
        # Передать исключение в GUI
        raise
    #endtry
    #------------------------------------------------------------------------
    #print opts
    # [('--output', 'hhhh.txt'), ('-v', '')]
    #------------------------------------------------------------------------
    #print args
    # ['uuu']
    #------------------------------------------------------------------------
    for op, naim in opts:
        if op in ("-h", "--help"):
            Usage()
        elif op in ("-o", "--output"):
            output = naim
        elif op in ("-c", "--copyright"):
            Copyright()
        elif op in ("-r", "--revision"):
            print __revision__
        #endif
    #endfor    
#enddef
#############################################################################
def Usage():
    # Выделение имени программы 
    sProgName = os.path.basename(sys.argv[0])
    #sProgName = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    print """Программа предназначена для того-то и того-то...

Запуск: 
   %s [опции] опции

Необязательные опции:
  -h, --help                  Показать настоящую справку
  -o FILE, --output=FILE      FILE - имя выходного файла   
  -v, --revision              Показать информацию о ревизии программы
  
Обязательные опции соединения с БД:
  -d, --driver=DRIVERNAME     Имя драйвера БД
  -s, --server=SERVERNAME     Имя сервера, на котором установлена СУБД
  -b, --base=BASENAME         Имя БД 
  -u, --username=LOGIN        Логин пользователя в СУБД
  -p, --password=PASSWORD     Пароль пользователя в СУБД
  -r, --readonly=TRUE         Режим "только для чтения" TRUE, иначе FALSE
  
Пример запуска:
   %s -d PostgreSQL Unicode -s zif-srv-7 -b power -с
  
"""  % (sProgName, sProgName)
#enddef    
#############################################################################
if __name__ == "__main__":
    # s - это аналог командной строки, чтобы не вызывать программу извне: 
    #------------------------------------------------------------------------
    s = "-ohhhh.txt  -h -r --copyright"
    #s = "-c"
    #s = "--copyright"
    #s = "--help"
    #s = "-r"
    #s = "--revision"
    #------------------------------------------------------------------------
    # ar - это аналог содержимого списка sys.argv
    ar = s.split()
    #------------------------------------------------------------------------
    Main(ar)
    #Main(sys.argv)
#endif
#############################################################################
"""
pg_dump dumps a database as a text file or to other formats.

Usage:
  pg_dump [OPTION]... [DBNAME]

General options:
  -f, --file=FILENAME      output file name
  -F, --format=c|t|p       output file format (custom, tar, plain text)
  -i, --ignore-version     proceed even when server version mismatches
                           pg_dump version
  -v, --verbose            verbose mode
  -Z, --compress=0-9       compression level for compressed formats
  --help                   show this help, then exit
  --version                output version information, then exit

Options controlling the output content:
  -a, --data-only             dump only the data, not the schema
  -b, --blobs                 include large objects in dump
  -c, --clean                 clean (drop) schema prior to create
  -C, --create                include commands to create database in dump
  -d, --inserts               dump data as INSERT commands, rather than COPY
  -D, --column-inserts        dump data as INSERT commands with column names
  -E, --encoding=ENCODING     dump the data in encoding ENCODING
  -n, --schema=SCHEMA         dump the named schema(s) only
  -N, --exclude-schema=SCHEMA do NOT dump the named schema(s)
  -o, --oids                  include OIDs in dump
  -O, --no-owner              skip restoration of object ownership
                              in plain text format
  -s, --schema-only           dump only the schema, no data
  -S, --superuser=NAME        specify the superuser user name to use in
                              plain text format
  -t, --table=TABLE           dump the named table(s) only
  -T, --exclude-table=TABLE   do NOT dump the named table(s)
  -x, --no-privileges         do not dump privileges (grant/revoke)
  --disable-dollar-quoting    disable dollar quoting, use SQL standard quoting
  --disable-triggers          disable triggers during data-only restore
  --use-set-session-authorization
                              use SESSION AUTHORIZATION commands instead of
                              ALTER OWNER commands to set ownership

Connection options:
  -h, --host=HOSTNAME      database server host or socket directory
  -p, --port=PORT          database server port number
  -U, --username=NAME      connect as specified database user
  -W, --password           force password prompt (should happen automatically)

If no database name is supplied, then the PGDATABASE environment
variable value is used.

Report bugs to <pgsql-bugs@postgresql.org>.
"""


