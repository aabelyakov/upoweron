#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 08.06.2015
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#----------------------------------------------------------------------------
# Текущий рабочий каталог - это каталог web2py
os.chdir('../../../..')
#print(os.getcwd())
# /home/aab/MyPython/web2py
#----------------------------------------------------------------------------
# Включение в список путей импорта sys.path - пути к каталогу web2py
sys.path.insert(0, os.getcwd())
#----------------------------------------------------------------------------
from applications.upoweron.modules import uLib as l
from applications.upoweron.models.u1_db import db
#############################################################################
# Создание хранилища для псевдо-сессии
ss = l.PseudoSs()
#----------------------------------------------------------------------------
ss.iObjId = 1
ss.sDt = "2015.05.20"
ss.xCp.M = 1 

print(l.ListId(db, ss, tbl="sht"))
print(l.ListId(db, ss, tbl="krp"))
print(l.ListId(db, ss, tbl="pdr"))
print(l.ListId(db, ss, tbl="lgh"))
#############################################################################
