# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 12.05.2009
# Purpose: Модуль для тестирования функции uLib.ToFloat(), служащей для 
# преобразования двух полей из ответа счётчика, содержащих, соответственно,
# целую и дробную части числа с плавающей точкой в параметр счётчика 
# типа float 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import os, sys
#############################################################################
# Переход в родительский каталог
os.chdir('..')
#----------------------------------------------------------------------------
# Вставка первым элементом списка - полного пути к родительскому каталогу
sys.path.insert(0, os.getcwd())
#############################################################################
import uLib
##########################################################################
print uLib.ToFloat("cd e0 20 f1", "f2 e5 ed e5")
# 3454017777.41
print uLib.ToFloat("12 13 14 15", "08 09 10 11")
# 303240213.135
print uLib.ToFloat("12 13", "08 09 10 11")
# 4627.13481166
##########################################################################







