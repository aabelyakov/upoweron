#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 23.03.2015
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
import sys
sys.path.append("/usr/local/lib/python2.7/dist-packages/ChartDirector")
from pychartdir import *
#############################################################################
# The data for the bar chart
data = [85, 156, 179.5, 211, 123]
#----------------------------------------------------------------------------
# The labels for the bar chart
labels = ["Mon", "Tue", "Wed", "Thu", "Fri"]
#----------------------------------------------------------------------------
# Create a XYChart object of size 800 x 800 pixels
c = XYChart(800, 800)
#----------------------------------------------------------------------------
# Set the plotarea at (60, 40) and of size 400 x 400 pixels
c.setPlotArea(60, 40, 700, 700)
#----------------------------------------------------------------------------
# Add a bar chart layer using the given data
c.addBarLayer(data)
#----------------------------------------------------------------------------
# Set the labels on the x axis.
c.xAxis().setLabels(labels)
#----------------------------------------------------------------------------
# output the chart
c.makeChart("XYChart.png")
#############################################################################
