# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 08.03.2009
# Module name: makeSysDsn.py
# Purpose: Создание системного bkb пользовательского DSN с помощью функции 
# из системной библиотеки windll.ODBCCP32.SQLConfigDataSource()
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
"""
Строка соединения для PostgreSQL (см. http://www.connectionstrings.com)
=============================================================================
Standard
Driver={PostgreSQL};Server=IpAddress;Port=5432;Database=myDataBase;
Uid=myUsername;Pwd=myPassword;  
-----------------------------------------------------------------------------
ANSI
Driver={PostgreSQL ANSI};Server=IpAddress;Port=5432;Database=myDataBase;
Uid=myUsername;Pwd=myPassword;  
-----------------------------------------------------------------------------
Unicode
Driver={PostgreSQL UNICODE};Server=IpAddress;Port=5432;Database=myDataBase;
Uid=myUsername;Pwd=myPassword;  
-----------------------------------------------------------------------------
SSL
Secure sockets layer for this driver only works from version 8.0 and above.
Driver={PostgreSQL ANSI};Server=IpAddress;Port=5432;Database=myDataBase;
Uid=myUsername;Pwd=myPassword;sslmode=require;
=============================================================================
Утилита "Администратор ODBC" для настройки ODBC соединения находится
в панели управления как пункт "Источники данных (ODBC)". 
На самом деле это обычная программа Odbcad32.exe.

Системная функция с помощью которой можно настроить источники данных ODBC
называется SQLConfigDataSource().

BOOL SQLConfigDataSource
(
    HWND hwndParent,		// Указатель на окно, вызвавшее функцию
    WORD fRequest,		// Команда
    LPCSTR lpszDriver,		// Имя драйвера БД
    LPCSTR lpszAttributes	// Атрибуты соединения с БД
);
Функция возвращает TRUE, если выполнена удачно и FALSE - если неудачно. 
=============================================================================
Команда может быть одной из следующих: 
ODBC_ADD_DSN    = 1      Добавить новый пользовательский DSN 
ODBC_CONFIG_DSN = 2      Конфигурировать существующий пользовательский DSN 
ODBC_REMOVE_DSN = 3      Удалить существующий пользовательский DSN 

ODBC_ADD_SYS_DSN    = 4  Добавить новый системный DSN
ODBC_CONFIG_SYS_DSN = 5  Конфигурировать существующий системный DSN
ODBC_REMOVE_SYS_DSN = 6  Удалить существующий системный DSN 

ODBC_REMOVE_DEFAULT_DSN = 7 Удалить DSN, заданный по умолчанию
"""
#############################################################################
import os, sys
#############################################################################
# Переход в родительский каталог
os.chdir('..')
#----------------------------------------------------------------------------
# Вставка первым элементом списка - полного пути к родительскому каталогу
sys.path.insert(0, os.getcwd())
#############################################################################
import ctypes, myutils
#############################################################################
# Внимание! DESCRIPTION и PASSWORD не прописываются при создании или
# модификации DSN
retCode = myutils.MakeSysDsn(
    "PostgreSQL Unicode", 
    SERVER="localhost", 
    DESCRIPTION="TestDB DSN", 
    DSN="TestDB", 
    DATABASE="astue",
    UID="postgres",
    PSW="cj.pybr"
)
#----------------------------------------------------------------------------
if retCode == True:
    print "Функция MakeSysDsn выполнена успешно"
else:
    print "Функция MakeSysDsn не выполнена"
#endif    
#----------------------------------------------------------------------------
print ("Проверка работы оператора print без обратного слеша "
       " и без +. "
       "Ещё одна строка.")
#############################################################################
