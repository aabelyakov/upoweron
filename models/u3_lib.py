#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 02.04.2015
# Purpose: 
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
#import uLib as l
#############################################################################
def IsRqsId():
    """
    @global ss [Storage] Хранилище данных сессии
    @global rq [Storage] Хранилище данных запроса
    -------------------------------------------------------------------------
    Проверка - выбран ли список запросов для опроса?
    """
    #------------------------------------------------------------------------
    if not ss.iRqsId:
        # Текущий список запросов не существует
        #--------------------------------------------------------------------
        # Флеш-сообщение, которое будет выдано на странице назначения
        # после перенаправления
        ss.flash = "Необходимо выбрать список запросов для опроса"
        #--------------------------------------------------------------------
        # Перенаправление на страницу выбора запросов
        redirect(URL(r=rq, c="requests", f='rqs_sel'))
    #endif
#enddef  
#############################################################################
