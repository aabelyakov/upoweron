#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 25.02.2010
# Purpose: Модель - Главное меню приложения uPowerOn
#############################################################################
# uPowerOn - АСТУЭ для счетчиков ННПО им.М.В.Фрунзе конструкции Южука А.Л.
#                         (С) А.А.Беляков 2010
#############################################################################
# Имя приложения
app = rq.application
#----------------------------------------------------------------------------
# Имя контроллера
ctr = rq.controller
#----------------------------------------------------------------------------
# Имя шаблона
view = rs.view
#----------------------------------------------------------------------------
# Заголовок приложения для базового шаблона
rs.title = "uPowerOn"
#----------------------------------------------------------------------------
# Подзаголовок приложения 
rs.subtitle = 'Автоматизированная Система Технического Учёта \
Электроэнергии'
#----------------------------------------------------------------------------
# Автор приложения 
rs.author = "Беляков Анатолий Алексеевич"
#----------------------------------------------------------------------------
# Узнай больше о мета-атрибутах на сайте:
# http://dev.w3.org/html5/markup/meta.name.html
rs.meta.author = 'А.А.Беляков <aabelyakov@mail.ru>'
rs.meta.description = 'a cool new app'
rs.meta.keywords = 'web2py, python, framework'
rs.meta.generator = 'Web2py Web Framework'
rs.meta.copyright = '(С) А.А.Беляков 2010'
#============================================================================
ADMIN_MENU = False
#----------------------------------------------------------------------------
# Проверим, что последний вошедший пользователь является членом группы adm,
# то есть group_id = ss.iAdmId
ADMIN_MENU = auth.has_membership(ss.iAdmId)
#----------------------------------------------------------------------------
# Главное меню приложения для пользователей из группы usr
rs.menu = [
    ['Начальная страница', False,  URL(
        app,
        "default",
        "index")],

    ['Выгруз. отчёт/диагр.', False, "", [
        ['Получасовые СМ', False, URL(
            app,
            'reports',
            'rpt_dwn_pwr')],           
    
        ['Базовые параметры', False, URL(
            app,
            'reports',
            'rpt_sel_obj',
            args='dwn')],
        ]
     ],
]
#============================================================================
def adm_menu():
    # Главное меню приложения для пользователей из группы adm
    #------------------------------------------------------------------------
    rs.menu = [
        ['Начальная страница', False,  URL(
            app,
            "default",
            "index")],
    
        ['Список б/а запросов', False, "", [
            ["Выбрать", False, URL(
                app,
                'requests',
                'rqs_sel')],
    
            ["Править", False, URL(
                app,
                'requests',
                'rqs_upd_atr')],
    
            ["Загрузить", False, URL(
                app,
                'requests',
                'rqs_upl')],
    
            ["Выгрузить", False, URL(
                app,
                "requests",
                "rqs_dwn")],
              
            ["Создать (Чтение энергии)", False, URL(
                app,
                "requests",
                "rqs_crt_enr")],
            
            ["Создать (Чтение СМ)", False, URL(
                app,
                "requests",
                "rqs_crt_pwr")],
            ]],
    
        ['Исполнители заданий', False, "", [
            ["Смотреть", False, URL(
                app,
                'workers',
                'wrk_view')],
            
            ["Запустить", False, URL(
                app,
                'workers',
                'wrk_start')],
    
            ["Удалить", False, URL(
                app,
                'workers',
                'wrk_del')],
            ]],
        
        ['Однократный опрос', False, URL(
            app,
            'opros',
            'opr_mode')],
        
        ["Повторитель опроса", False, URL(
            app,
            'repeaters',
            'rep_frm')],
        
        ['Планировщик опроса', False, "", [
            ["Смотреть", False, URL(
                app,
                'planners',
                'pln_view')],
            
            ["Запустить", False, URL(
                app,
                'planners',
                'pln_start')],
    
            ["Удалить", False, URL(
                app,
                'planners',
                'pln_del')],
            ]],
        
        ['Удалить устаревшие СМ', False, URL(
            app,
            'records',
            'rec_del')],
                
        ['Экспортир. данные', False, "", [
            ["Из одной таблицы", False, URL(
                app,
                'csvfile',
                'csv_exp_one')],
    
            ['Изо всех справ. таблиц', False, URL(
                app,
                'csvfile',
                'csv_exp_all')],
            ]],
    
        ['Импортир. данные', False, "", [
            ['В одну таблицу', False, URL(
                app,
                'csvfile',
                'csv_imp_one')],
    
            ['Во все справ. таблицы', False, URL(
            app,
            'csvfile',
            'csv_imp_all')],
        ]],
        
        ['Удалить все данные', False, "", [
            ["Из одной таблицы", False, URL(
                app,
                'edit',
                'edt_trn_one')],
    
            ['Изо всех опер.таблиц', False, URL(
                app,
                'edit',
                'edt_trn_all')],
            ]],
   
        ['Править таблицу', False,  URL(
            app,
            "edit",
            "edt_sel_spr")],
    
        ['Смотреть таблицу', False,  URL(
            app,
            "edit",
            "edt_sel_tbl")],
        
        ['Смотреть csv-файл', False,  URL(
            app,
            "csvfile",
            "csv_vie_all")],
        
        ['Отправить почту', False,  URL(
            app,
            "mail",
            "mai_pusk")],

        ['Править конфиг', False,  URL(
            app,
            "edit",
            "edt_cfg_par")],
        
        ['Очередь заданий', False, "", [
            ['Смотреть', False,  URL(
                app,
                "opros",
                "opr_vie_que")],
            
            ['Чистить', False,  URL(
                app,
                "opros",
                "opr_clr_que")],
            ]],
        
        ['Создать отчёт/диагр.', False, "", [
            ['Получасовые СМ', False,  URL(
                app,
                "reports",
                "rpt_crt_pwr")],
            
            ['Базовые параметры', False, URL(
                app,
                'reports',
                'rpt_sel_obj',
                args='crt')],
            ]],
    
        ['Выгруз. отчёт/диагр.', False, "", [
            ['Получасовые СМ', False, URL(
                app,
                'reports',
                'rpt_dwn_pwr')],           
        
            ['Базовые параметры', False, URL(
                app,
                'reports',
                'rpt_sel_obj',
                args='dwn')],
            ]],
        
                
        ["Сайт", False, URL(
            "admin",
            "default",
            "site")],
            
        ['Тесты', False, "", [
            ['cdz-cdp', False, URL(
                app,
                'tests',
                'test1')],
                   
            ['cdz-cdp-par', False, URL(
                app,
                'tests',
                'test2')],

            ['Left', False, URL(
                app,
                'tests',
                'test3')],
            
            ['Xlsx', False, URL(
                app,
                'tests',
                'test24')],
            
            ['Множ.Выбор', False, URL(
                app,
                'tests',
                'test5')],

            ['Планировщик', False, URL(
                app,
                'tests',
                'test26')],
            
            ['HTML', False, URL(
                app,
                'tests',
                'test18')],
            
            ['CSV', False, URL(
                app,
                'tests',
                'test19')],
                        
            ['Sht', False, URL(
                app,
                'tests',
                'test28')],
            
            ['Log', False, URL(
                app,
                'tests',
                'test29')],
            
            ['XML', False, URL(
                app,
                'tests',
                'test30')],
            
            ['Geraldo', False, URL(
                app,
                'tests',
                'test4')],
            
            ['Комби', False, URL(
                app,
                'tests',
                'test6')],
            
            ['Почта', False, URL(
                app,
                'tests',
                'test7')],
            
            ['XML1', False, URL(
                app,
                'tests',
                'test8')],
            
            ['XML2', False, URL(
                app,
                'tests',
                'test9')],
            
            ['Диаграмма', False, URL(
                app,
                'tests',
                'test10')],
            
            ['Test', False, URL(
                app,
                'tests',
                'bottle')],
            
            ['Update', False, URL(
                app,
                'tests',
                'updat')],
            
            ['Manage', False, URL(
                app,
                'tests',
                'manage',
                args=['sht', 2])],
            
            ['Three', False, URL(
                app,
                'tests',
                'three', args=1)],

            ['Привет', False, URL(
                app,
                'tests',
                'form')], 
        ]],
 
        ['Смотреть лог-файл', False,  URL(
            app,
            "edit",
            "edt_vie_log1")],
    ]
#enddef    
#----------------------------------------------------------------------------
if ADMIN_MENU:
    # Установлен флаг - Показать меню администратора
    #------------------------------------------------------------------------
    # Вызов функции показа главного меню приложения для группы adm
    adm_menu()
#endif
#############################################################################

